import React, {Component} from "react";
import './style.css';

class Display extends Component {
    render() {
        return (
            <input className="calculator-display" type="text" value={this.props.result} readOnly />
        );
    }
}

export default Display;


