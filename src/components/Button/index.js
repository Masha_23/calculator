import React, {Component} from "react";
import './style.css';

class Button extends Component {

    render() {
        const { value } = this.props;

        return (
          <button
              className="calculator-button"
              onClick={this.props.onClick}
          >
              {value}
          </button>
        );
    }
}


export default Button;



