import React, {Component} from "react";
import Button from '../Button';
import Display from '../Display';
import './style.css';

class Board extends Component
{
    state = {
        result : ""
    };

    handleClick(button) {
        if (button === "Clear") {
            this.reset();
        }
        else if (button === "=") {
            this.calculate();
        }
        else {
            this.setState({result : this.state.result + button});
        }
    };

    reset() {
        this.setState({result : ""});
    }

    calculate() {
        this.setState({
            result : eval(this.state.result)
        });
    }

    renderButton(value) {
        return (
            <Button
                value={value}
                onClick={() => this.handleClick(value)}
            />
        );
    };

    render(){
        return(
            <div className="calculator-board">
                <Display result={this.state.result}/><br/>
                <div className="calculator-board-row">
                    {this.renderButton("7")}
                    {this.renderButton("8")}
                    {this.renderButton("9")}
                    {this.renderButton("+")}
                </div>
                <div className="calculator-board-row">
                    {this.renderButton("4")}
                    {this.renderButton("5")}
                    {this.renderButton("6")}
                    {this.renderButton("-")}
                </div>
                <div className="calculator-board-row">
                    {this.renderButton("1")}
                    {this.renderButton("2")}
                    {this.renderButton("3")}
                    {this.renderButton("*")}
                </div>
                <div className="calculator-board-row">
                    {this.renderButton("Clear")}
                    {this.renderButton("0")}
                    {this.renderButton("=")}
                    {this.renderButton("/")}
                </div>
            </div>
        );
    }
}


export default Board;



